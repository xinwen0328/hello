pipeline {
    agent none
    environment {
            NEXUS_URL="http://172.31.43.138:8081"
            NEXUS_CREDENTIAL=credentials('nexus-credentials')
            SONAR_CREDENTIAL=credentials('sonar')
            SONAR_HOST_URL="http://35.85.202.210:9000"
    }
    parameters {
           choice(choices: ['pi','linux','mac'], description: 'Which target platform?', name: 'TARGET')
    }
    stages {
        stage('Prepare'){
            agent any
            steps{
                script {
                    env.GIT_COMMIT_TIME=sh(script: "/usr/bin/git log --format=format:%ct -1 $GIT_COMMIT",returnStdout: true).trim()
                    echo env.GIT_COMMIT_TIME
                }
            }
        }
        stage('Compile') {
            agent { 
                docker { 
                    image 'golang:latest'
                    args ' -u 0:0 '
                } 
            }
            steps {

                sh 'go mod tidy'
                
                sh '''
                for TARGET in $(echo pi linux mac);
                do
                   # Set binary name
                   TARGET_MAIN="ds-core-main"

                   # Set build environment variables 
                   case $TARGET in
                     "pi") 
                          TARGET_GO="CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=6"
                      ;;
                      "linux") 
                          TARGET_GO="CGO_ENABLED=0 GOOS=linux GOARCH=amd64"
                      ;;
                     "mac") 
                          TARGET_GO="CGO_ENABLED=0 GOOS=darwin GOARCH=amd64"
                      ;;
                   esac
                   
                   # Set output path
                   TARGET_DIR="${WORKSPACE}/bin/${TARGET}"
                   if [ ! -d "${TARGET_DIR}/ds-core" ]; then
                      mkdir -p "${TARGET_DIR}/ds-core"
                   fi

                   # Build start here 
                   
                   CMD="$TARGET_GO go build -o ${TARGET_DIR}/ds-core/${TARGET_MAIN}"
                   eval $CMD 

                   # Package start here 
                   cp sysConfig.json ${TARGET_DIR}/ds-core/.
                   cp run.sh ${TARGET_DIR}/ds-core/.
                   tar -cvf ./ds-core-${TARGET}-${GIT_COMMIT_TIME}.tar -C ${TARGET_DIR} ds-core
            
                   # Release to Nexus... 
                   # curl -v -u $NEXUS_CREDENTIAL --upload-file ds-core-${TARGET}-${GIT_COMMIT_TIME}.tar ${NEXUS_URL}/repository/mygolang/ds-core-${TARGET}-${GIT_COMMIT_TIME}.tar
                done
                   
                '''

                sh '''
                       # run golangci-lint
                       curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.42.1
                       golangci-lint run -v --out-format checkstyle > ./golangcilint.xml
                '''      
            }
        }
        stage('Code Analysis') {
            options { skipDefaultCheckout() }
            agent { 
                docker { 
                    image 'sonarsource/sonar-scanner-cli'
                    args ' -u 0:0 --entrypoint=\'\' --link sonarqube:sonarqube '
                } 
            }
            
            steps {
                   sh '''
                    remainder="${JOB_NAME%/*}"
                    SERVICE_NAME="${remainder##*/}"
                    remainder="${remainder%/*}"
                    PROJECT_NAME="${remainder##*/}"
                    SONAR_PROJECTKEY="${PROJECT_NAME}_${SERVICE_NAME}"
                    
                    sonar-scanner  -Dsonar.projectKey=${SONAR_PROJECTKEY} \
                                   -Dsonar.login=${SONAR_CREDENTIAL} \
                                   -Dsonar.host.url=${SONAR_HOST_URL} \
                                   -Dsonar.sources=.  \
                                   -Dsonar.exclusions=**/*_test.go,**/vendor/** \
                                   -Dsonar.tests=. \
                                   -Dsonar.test.inclusions=**/*_test.go \
                                   -Dsonar.test.exclusions=**/vendor/** \
                                   -Dsonar.sourceEncoding=UTF-8  \
                                   -Dsonar.go.golangci-lint.reportPaths=./golangcilint.xml
                    '''  
            }   
        }
    }
}
